/**
 * Text
 *
 * Текстовая страница
 *
 * @category	template
 * @version 	1.0
 * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @internal	@lock_template 0
 * @internal    @installset sample
 */
 
[*content*]