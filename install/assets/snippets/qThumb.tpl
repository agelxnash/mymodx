//<?php
/**
 * qThumb
 * 
 * PHPThumb creates thumbnails and altered images on the fly and caches them
 *
 * @category 	snippet
 * @version 	1.0
 * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @internal	@properties &queue=Использовать очередь;list;true,false;false
 * @internal	@modx_category API
 * @internal    @installset base, sample
 * @author      Agel_Nash
 */

return require MODX_BASE_PATH.'assets/snippets/phpthumb/qThumb.snippet.php';
