//<?php
/**
 * ajaxSend
 * 
 * Ajax отправка форм
 *
 * @category 	snippet
 * @version 	1.0
 * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @internal	@properties 
 * @internal	@modx_category API
 * @internal    @installset base, sample
 * @author      Agel_Nash
 */

return require MODX_BASE_PATH.'assets/snippets/snippet.ajaxSend.php';
