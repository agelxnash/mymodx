﻿/**
 * to_email
 *
 * E-mail получателя писем
 *
 * @category        tv
 * @name            to_email
 * @internal        @caption E-mail получателя писем
 * @internal        @input_type text
 * @internal        @input_options
 * @internal        @input_default 
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @template_assignments Config
 * @internal        @installset sample
 */