﻿/**
 * SEOTitle
 *
 * SEO заголовок
 *
 * @category        tv
 * @name            SEOTitle
 * @internal        @caption SEO заголовок
 * @internal        @input_type text
 * @internal        @input_options
 * @internal        @input_default 
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @template_assignments Text
 * @internal        @modx_category SEO
 * @internal        @installset sample
 */