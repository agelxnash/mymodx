﻿/**
 * showInMain
 *
 * Показывать на главной?
 *
 * @category        tv
 * @name            showInMain
 * @internal        @caption Показывать на главной?
 * @internal        @input_type checkbox
 * @internal        @input_options Да==1
 * @internal        @input_default 
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @template_assignments
 * @internal        @modx_category
 * @internal        @installset sample
 */