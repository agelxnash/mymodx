﻿/**
 * hideTitle
 *
 * Спрятать заголовок страницы?
 *
 * @category        tv
 * @name            hideTitle
 * @internal        @caption Спрятать заголовок страницы?
 * @internal        @input_type checkbox
 * @internal        @input_options Да==1
 * @internal        @input_default 
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @template_assignments Text
 * @internal        @modx_category
 * @internal        @installset sample
 */