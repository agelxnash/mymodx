﻿/**
 * HtmlInLine
 *
 * Сжимать HTML в 1 строчку
 *
 * @category        tv
 * @name            HtmlInLine
 * @internal        @caption Сжимать HTML в 1 строчку
 * @internal        @input_type option
 * @internal        @input_options Да==1||Нет==0
 * @internal        @input_default 1
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @template_assignments Text
 * @internal        @modx_category SEO
 * @internal        @installset sample
 */