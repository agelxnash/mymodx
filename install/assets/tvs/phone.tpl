﻿/**
 * phone
 *
 * Контактный телефон
 *
 * @category        tv
 * @name            phone
 * @internal        @caption Контактный телефон
 * @internal        @input_type text
 * @internal        @input_options
 * @internal        @input_default 
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @template_assignments Config
 * @internal        @installset sample
 */