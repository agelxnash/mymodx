﻿/**
 * SEODesc
 *
 * SEO описание документа
 *
 * @category        tv
 * @name            SEODesc
 * @internal        @caption SEO описание документа
 * @internal        @input_type textareamini
 * @internal        @input_options
 * @internal        @input_default 
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @template_assignments Text
 * @internal        @modx_category SEO
 * @internal        @installset sample
 */