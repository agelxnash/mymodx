// <?php 
/**
 * Консоль
 * 
 * MODX Консоль
 * 
 * @category	module
 * @version 	1.0
 * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @internal	@properties
 * @internal	@guid d5a6b362806cf3fb330fdd3274a88a9a
 * @internal	@modx_category API
 */

require_once MODX_BASE_PATH . 'assets/modules/console/module.tpl.php';