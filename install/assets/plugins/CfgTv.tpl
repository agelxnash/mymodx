//<?php
/**
 * CfgTv
 *
 * Конфигурация в ТВ параметрах
 *
 * @version     1.1.0
 * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @category    plugin
 * @internal    @legacy_names CfgTv
 * @internal    @properties &ids=ID ресурсов настроек;text;2 &prefix=Префикс;text;cfg_
 * @internal    @modx_category API
 * @internal    @events OnBeforeDocFormSave
 */

require_once MODX_BASE_PATH . "assets/plugins/CfgTv.plugin.php";