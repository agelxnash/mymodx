//<?php
/**
 * replaceTemplate
 *
 * Загрузка шаблонов из файлов
 *
 * @version     1.0.0
 * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @category    plugin
 * @author  	Agel Nash <modx@agel-nash.ru>
 * @internal    @legacy_names replaceTemplate
 * @internal    @properties &debug=Debug;list;true,false;false &conditional=Conditional;list;true,false;true
 * @internal    @modx_category API
 * @internal    @events OnAfterLoadDocumentObject,OnLoadWebDocument,OnWebPageInit,OnWebPagePrerender,OnCacheUpdate,OnManagerPageInit,OnPageNotFound
 * @internal    @disabled 1
 */

require MODX_BASE_PATH.'assets/plugins/plugin.replaceTemplate.php';