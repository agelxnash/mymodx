//<?php
/**
 * LoadElement
 *
 * Загрузка элементов из файлов
 *
 * @version     1.0.0
 * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @category    plugin
 * @author  	Agel Nash <modx@agel-nash.ru>
 * @internal    @legacy_names LoadElement
 * @internal    @properties &extChunk=Расширения чанков (<i>через запятую</i>);input;txt,html &extSnippet=Расширения сниппетов (<i>через запятую</i>);input;php &pathElement=Папка с элементами (<i>относительно корня сайта</i>);input;assets/element/
 * @internal    @modx_category API
 * @internal    @events OnWebPageInit,OnManagerPageInit,OnPageNotFound
 */

require MODX_BASE_PATH.'assets/plugins/plugin.LoadElement.php';