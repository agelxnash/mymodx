//<?php
/**
 * fixUrl
 *
 * Защита AJAX документов от прямого обращения
 *
 * @version     1.0.0
 * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @category    plugin
 * @author  	Agel Nash <modx@agel-nash.ru>
 * @internal    @legacy_names fixUrl
 * @internal    @properties
 * @internal    @modx_category API
 * @internal    @events OnWebPagePrerender
 */

require MODX_BASE_PATH.'assets/plugins/plugin.fixUrl.php';