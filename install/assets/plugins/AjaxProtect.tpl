//<?php
/**
 * Ajax Protect
 *
 * Защита AJAX документов от прямого обращения
 *
 * @version     1.0.0
 * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @category    plugin
 * @author  	Agel Nash <modx@agel-nash.ru>
 * @internal    @legacy_names Ajax Protect
 * @internal    @properties &parent=ID папки с ajax доками;string;
 * @internal    @modx_category API
 * @internal    @events OnWebPageInit,OnWebPagePrerender,OnPageNotFound
 */

require MODX_BASE_PATH.'assets/plugins/plugin.ajax_protect.php';