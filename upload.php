<?php
define('MODX_API_MODE', true);
include_once(dirname(__FILE__) . "/index.php");
$modx->db->connect();
/** Принудительно обновляем конфиг */
$modx->config = array();
unlink(MODX_BASE_PATH.'assets/cache/siteCache.idx.php');
$modx->getSettings();

$modx->documentMethod = "id";
$modx->documentIdentifier = isset($_REQUEST['pageID']) ? (int)$_REQUEST['pageID'] : 1;
$modx->documentObject = $modx->getDocumentObject('id', $modx->documentIdentifier);

$modx->invokeEvent("OnWebPageInit");

if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest')){
	$modx->sendErrorPage();
}

if( !empty($_SERVER['HTTP_ORIGIN']) ){
	// Enable CORS
	header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
	header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type');
}
if( $_SERVER['REQUEST_METHOD'] == 'OPTIONS' ){
	exit;
}

$uploadDir = MODX_BASE_PATH.'assets/cache/upload/';

if( strtoupper($_SERVER['REQUEST_METHOD']) == 'POST' ){
	$files	= \FileAPI::getFiles(); // Retrieve File List
	$modx->fs->makeDir($uploadDir, $modx->config['new_folder_permissions']);
	if ($files['upload']['error'] == UPLOAD_ERR_OK) {
		$tmp_name = $files["upload"]["tmp_name"];
		$name = $modx->stripAlias($_FILES["upload"]["name"]);

		$name = $modx->fs->getInexistantFilename("$uploadDir/$name");
		$ext = $modx->fs->takeFileExt($uploadDir/$name);
		if (!in_array($ext, array('php', 'html', 'htaccess', 'inc' ))) {
			if (@move_uploaded_file($tmp_name, $uploadDir.$name)) {
				$files = $modx->fs->relativePath($uploadDir.$name);
			}
		} else {
			$files['upload']['error'] = 101;
		}
	}

	$json	= $files;

	// JSONP callback name
	$jsonp	= isset($_REQUEST['callback']) ? trim($_REQUEST['callback']) : null;

	// Server response: "HTTP/1.1 200 OK"
	\FileAPI::makeResponse(array(
		'status' => FileAPI::OK
		, 'statusText' => 'OK'
		, 'body' => $json
	), $jsonp);
	exit;
}