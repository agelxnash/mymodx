<?php
if(!function_exists('is_phone')){
	function is_phone($phone){
		return preg_match('/^\+\d{1} \(\d{3}\) \d{3}\-\d{2}-\d{2}$/is', $phone);
	}
}

if(!function_exists('num_format')){
	function num_format($data, $dec = 0){
		return number_format($data, $dec, ",", " ");
	}
}