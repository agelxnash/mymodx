<?php namespace ProjectName;

class DLPrepare{
	public static function sitemap($data, $modx, $_DL, $_eDL){
		if($data['content'] == '#' && $data['type'] == 'reference'){
			$_DL->renderTPL = $_DL->getCfgDef('noLinkTPL');
		}
		return $data;
	}
	public static function sitemapXML($data, $modx, $_DL, $_eDL){
		if($data['content'] == '#' && $data['type'] == 'reference'){
			$data = false;
		}else{
			$data['url'] = $modx->makeUrl($data['id'], '', '', 'full');
		}
		return $data;
	}
}