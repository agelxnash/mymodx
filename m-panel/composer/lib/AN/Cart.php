<?php namespace AN;

include_once(MODX_BASE_PATH."assets/lib/APIHelpers.class.php");

abstract class Cart{
    public $nameSession = 'ANcart';
    protected $modx = null;

    public function __construct($modx){
        $this->modx = $modx;
    }
    /**
    * ID товаров в корзине через запятую
    */
	public function Item($data = null, $implode = true) {
        $doc = (isset($data) && is_array($data) ? $data : \APIhelpers::getkey($_SESSION, $this->nameSession, array()));
        $document = array();
        foreach($doc as $key=>$item){
            preg_match("/^doc-(\d+)/i",$key,$tmp);
            if(isset($tmp[1]) && (int)$tmp[1]>0){
                $document[]=(int)$tmp[1];
            }
        }
        return $implode ? implode(",", $document) : $document;
    }

    /**
    * Всего уникальных позиций в корзине (1 телефон + 3 пива = 2)
    */
    public function TotalItem($doc = null) {
        $data= $this->Item();
        if(empty($data)) return 0;

        $data = explode(",",$data);
        $count = 0;
        foreach($data as $item){
            $count += ($this->checkItem($item) ? 1 : 0);
        }
        return $count;
    }

    /**
    * Всего товаров в корзине (1 телефон + 3 пива = 4)
    */
    public function FullTotalItem($doc = null) {
        $data= $this->Item();
        if(empty($data)) return 0;

        $data = explode(",",$data);

        $count = 0;
        foreach($data as $item){
            $count += $this->countItem($item);
        }
        return $count;
    }

    public function countItem($doc) {
        $count = 0;
        if($this->checkItem($doc)){
            $count = (int)$this->getItem($doc);
        }
        return $count;
    }

    public function checkItem($doc) {
        return ($this->getItem($doc) != null);
    }

    protected function getItem($doc) {
        $docList = \APIhelpers::getkey($_SESSION, $this->nameSession, array());
        return \APIhelpers::getkey($docList, 'doc-'.$doc, null);
    }

    /**
    * Общая стоимость товаров в корзине
    */
    public function FullPrice($doc = null) {
        $money = 0;
        if(is_null($doc)){
            $doc = $this->Item();
        }
        if(is_scalar($doc)){
            $doc = explode(",",$doc);
        }else{
            $doc = array();
        }
        foreach($doc as $itemID){
            $count = (int)$this->getItem($itemID);
            if($count > 0){
                $money += $this->ItemPrice($itemID) * $count;
            }
        }
        return $money;
    }

    /**
    * Стоимость товара
    */
    abstract function ItemPrice($item);

    /** метод добавления товара в корзину */
    public function addItemCart($id, $count = 1){
        $total = (int)$this->getItem($id);
        $this->setItemCart($id, $total + $count);
    }

    public function setItemCart($id, $count = 1){
        $_SESSION[$this->nameSession]['doc-'.$id] = $count;
    }

    /** метод очистки корзины */
    public function clearCart(){
        $_SESSION[$this->nameSession] = array();
    }

     /** метод удаления товара из корзины */
    public function delItemCart($id, $count = 1){
        $total = $this->getItem($id);
        if($total !== null){
            if($total-$count > 0){
                $_SESSION[$this->nameSession]['doc-'.$id] = $total-$count;
            }else{
                $this->fullDeleteItemCart($id);
            }
        }
    }

    public function fullDeleteItemCart($id){
        $total = $this->getItem($id);
        if($total !== null){
            unset($_SESSION[$this->nameSession]['doc-'.$id]);
        }
    }
}