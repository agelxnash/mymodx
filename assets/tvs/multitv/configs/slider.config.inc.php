<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'image' => array(
        'caption' => 'Слайд',
        'type' => 'image'
    ),
    'title' => array(
        'caption' => 'Заголовок',
        'type' => 'text'
    ),
    'desc' => array(
        'caption' => 'Описание',
        'type' => 'richtext'
    ),
    'link' => array(
        'caption' => 'Ссылка',
        'type' => 'link'
    )
);

$settings['configuration'] = array(
    'enablePaste' => false,
    'enableClear' => false
);