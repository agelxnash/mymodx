<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'image' => array(
        'caption' => 'Сертификат',
        'type' => 'image'
    ),
    /* 'thumb' => array(
        'caption' => 'Thumbnail',
        'type' => 'thumb',
        'thumbof' => 'image'
    ), */
    'title' => array(
        'caption' => 'Заголовок',
        'type' => 'text'
    )
);

$settings['configuration'] = array(
    'enablePaste' => false,
    'enableClear' => false
);