<?php

/**************************************/
/** HtmlInLine plugin for MODX Revo
*
* @version 1.1
* @author Borisov Evgeniy aka Agel Nash (agel-nash@xaker.ru)
*
* @category plugin
* @internal @event OnWebPagePrerender
* @internal @modx_category HTML-code
*
*/
/*************************************/
$e =&$modx->event;

$HtmlInLine = isset($modx->documentObject['HtmlInLine']) ? $modx->documentObject['HtmlInLine'] : '';
$HtmlInLine = (is_array($HtmlInLine) && isset($HtmlInLine[1])) ? $HtmlInLine[1] : 0;

switch ($e->name) {
    case "OnWebPagePrerender":{
        if($HtmlInLine){
            $content = $modx->documentOutput;
            $filters = array(
            	'/<!--([^\[|(<!)].*)-->/i'		=> '', // Remove HTML Comments (breaks with HTML5 Boilerplate)
	            '/(?<!\S)\/\/\s*[^\r\n]*/'	=> '', // Remove comments in the form /* */
	            '/\s{2,}/'			=> ' ', // Shorten multiple white spaces
	            '/(\r?\n)/'			=> '', // Collapse new lines
	        );

	        $content = preg_replace(array_keys($filters), array_values($filters), $content);
            $modx->documentOutput = $content;
        }
        break;
    }
}