<?php
/**
 * QueuePHPThumb
 *
 * @category  snippet
 * @version 	1.0
 * @license 	GNU General Public License (GPL), http://www.gnu.org/copyleft/gpl.html
 * @author Agel_Nash <Agel_Nash@xaker.ru>
 * @internal	@properties &queue=������������ �������;list;true,false;true
 *
 * [[phpthumb? &input=`[+tvimagename+]` &options=`w_255,h=200` &queue=`false`]]
 */
  
if(!defined('MODX_BASE_PATH')){die('What are you doing? Get out of here!');}

require_once MODX_BASE_PATH."/assets/snippets/phpthumb/QueuePHPThumb.class.php";
$qthumb = new QueuePHPThumb($modx);
$qthumb->init($modx->Event->params);
return $qthumb->makeFile();