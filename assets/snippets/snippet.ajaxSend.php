<?php
//[!ajaxSend? &id=`review` &form=`FORM.addReview` &alertTpl=`59` &alertMode=`page` &messageTpl=`EMAIL.addReview` &subject=`@CODE: Новый отзыв` &toMail=`modx@agel-nash.ru` &counterName=`count_send_mail`!]

if(!is_array($modx->event->params)) $modx->event->params = array();
$selfParams = $modx->event->params;

include_once(MODX_BASE_PATH."assets/snippets/DocLister/lib/DLTemplate.class.php");
include_once(MODX_BASE_PATH."assets/lib/APIHelpers.class.php");
$DLTemplate = \DLTemplate::getInstance($modx);

$out = '';
$flag = false;
if(strtolower(\APIhelpers::getkey($_SERVER, 'REQUEST_METHOD')) == 'post'){
	$modx->jsonMode = (strtolower(\APIhelpers::getkey($_SERVER, 'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest');
	$modx->jsonMixed = array(
		'replace' => \APIhelpers::getkey($selfParams, 'replaceHtml'),
	);
	$senderClass = \APIhelpers::getkey($selfParams, 'sender', '\\AN\\Sender');
	$sender = new $senderClass(
		$modx,
		\APIhelpers::getkey($selfParams, 'subject'),
		\APIhelpers::getkey($selfParams, 'toMail'),
		\APIhelpers::getkey($selfParams, 'messageTpl'),
		\APIhelpers::getkey($selfParams, 'counterName')
		);
	$sender->setAlert(\APIhelpers::getkey($selfParams, 'alertMode', 'chunk'), \APIhelpers::getkey($selfParams, 'alertTpl'));
	$sender->filesField = \APIhelpers::getkey($selfParams, 'filesField');

	$flag = $sender->send(is_array($_POST) ? $_POST : array());
	if ($flag === true) {
		$out = $sender->renderAlert();
	} else {
		$errorItemTpl = \APIhelpers::getkey($selfParams, 'errorItemTpl');
		$tmp = $DLTemplate->getChunk($errorItemTpl);
		if(empty($tmp)){
			$errorItemTpl = '@CODE: [+error+]<br />';
		}
		switch(true){
			case is_array($flag):{
				$countErrors = count($flag);
				foreach ($flag as $err) {
					$out .= $DLTemplate->parseChunk($errorItemTpl, array(
						'error' => $err,
						'count_errors' => $countErrors
					));
				}
				$errorType = 'Необходимо проверить корректность заполнения полей';
				$flag = false;
				break;
			}
			case is_object($flag):{
				$countErrors = count($flag->errors);
				foreach($flag->errors as $err){
					$out .= $DLTemplate->parseChunk($errorItemTpl, array(
						'error' => $err,
						'count_errors' => $countErrors
					));
				}
				$errorType = $flag->type;
				$flag = false;
				break;
			}
			default:{
				$countErrors = 0;
				$errorType = 'Не удалось отправить сообщение';
				break;
			}
		}

		$errorWrapTpl = \APIhelpers::getkey($selfParams, 'errorWrapTpl');
		$tmp = $DLTemplate->getChunk($errorWrapTpl);
		if(empty($tmp)){
			$errorWrapTpl = '@CODE: <h1>[+errorType+]</h1>[+errors+]';
		}
		if (!empty($errorWrapTpl)) {
			$out = $DLTemplate->parseChunk($errorWrapTpl, array(
				'errors' => $out,
				'errorType' => $errorType,
				'count_errors' => $countErrors
			));
		}
	}
	$modx->jsonStatus = $flag ? 'ok' : 'error';
}else{
	$out = $DLTemplate->parseChunk(\APIhelpers::getkey($selfParams, 'form'), array(), true);
}
return $out;