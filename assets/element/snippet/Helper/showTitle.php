<?php
$out = '';
$modx->event->params = is_array($modx->event->params) ? $modx->event->params : array();

$id = get_key($modx->event->params, 'id');
if(!empty($id)){
	$flag = isset($modx->documentObject[$id][1]) ? $modx->documentObject[$id][1] : '';
}else{
	$flag = false;
}
if(empty($flag)){
	$tpl = isset($tpl) ? $tpl : '@CODE: <h1>[+title+]</h1>';
	$data = $modx->documentObject;
	$data['title'] = get_key($data, 'menutitle', get_key($data, 'pagetitle', ''), function($val){
		return !empty($val);
	});
	$out = $modx->tpl->parseChunk($tpl, array_merge($data, $modx->event->params));
}
return $out;