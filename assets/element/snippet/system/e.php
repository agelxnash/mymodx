<?php
$input = get_key($modx->event->params, 'input', '', 'is_scalar');
$source = get_key($modx->event->params, 'source', 'doc', function($val){
	return in_array($val, array('doc', 'config'));
});
switch($source){
	case 'doc':{	
		$data = $modx->runSnippet('DDocInfo', array(
			'id' => get_key($modx->event->params, 'id', $modx->documentObject['id'], 'is_scalar'),
			'field' => $input
		));
		break;
	}
	case 'config':{
		$data = $modx->getConfig($input);
		break;
	}
}
return e($data);