<?php
/**
 * copyright
 * [[copyright? &sep=`-` &date=`2010`]]
 * Автоматическая смена копирайтов
 *
 * @category  snippet
 * @version   0.1
 * @license   GNU General Public License (GPL), http://www.gnu.org/copyleft/gpl.html
 * @author Agel_Nash <Agel_Nash@xaker.ru>
 */
$now=date("Y");
$out='';
if(isset($date) && $date!=$now){
    $out.=$date;
    if(isset($sep)){
        $out.=$sep;
    }
}
$out.=$now;
return $out;