<?php
/**
 * Версия MODX
 */
if(!defined('MODX_BASE_PATH')){die('What are you doing? Get out of here!');}
$input=isset($input) ? $input : 'full_appname';
$data=$modx->getVersionData();
return isset($data[$input]) ? $data[$input] : '';