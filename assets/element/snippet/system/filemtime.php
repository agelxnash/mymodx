<?php
return (empty($file) || !file_exists(MODX_BASE_PATH . $file)) ? 0 : filemtime(MODX_BASE_PATH . $file);