<?php
/**
 * Кнопки поделиться
 */
if(empty($url)){
    $url = $modx->makeUrl($modx->documentObject['id'], '', '', 'full');
}

if(empty($title)) {
    $title = isset($modx->documentObject['SEOTitle'][1]) ? $modx->documentObject['SEOTitle'][1] : '';
}
if(empty($title)){
    $title = $modx->documentObject['pagetitle'];
}

$config = is_array($modx->event->params) ? $modx->event->params : array();
$config['url'] = $url;
$config['titleEncode'] = urlencode($title);
$config['urlEncode'] = urlencode($url);

$links = isset($config['links']) ? $config['links'] : 'print,pdf,vk,od,gl,fb,tw,ml';
$links = explode(",", $links);
$href = array(
    'print' => $config['url'].'?save=print',
    'pdf' => $config['url'].'?save=pdf',
    'vk' => 'http://vkontakte.ru/share.php?url='.$config['urlEncode'],
    'od' => 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st._surl='.$config['urlEncode'].'&title='.$config['titleEncode'],
    'gp' => 'https://plus.google.com/share?url='.$config['urlEncode'],
    'fb' => 'http://www.facebook.com/sharer/sharer.php?s=100&p[url]='.$config['urlEncode'].'&p[title]='.$config['titleEncode'],
    'tw' => 'https://twitter.com/intent/tweet?text='.$config['titleEncode'].'&url='.$config['urlEncode'],
    'ml' => 'http://connect.mail.ru/share?share_url='.$config['urlEncode'],
    'ln'   => 'http://www.linkedin.com/shareArticle?mini=true&url='.$config['urlEncode'].'&title='.$config['titleEncode']//.'&summary={articleSummary}'
);
$title = array(
   'print' => 'Версия для печати',
    'pdf' => 'Сохранить в PDF',
    'vk' => 'Поделиться ВКонтакте',
    'od' => 'Поделиться в Одноклассниках',
    'gp' => 'Поделиться в Google+',
    'fb' => 'Поделиться в Facebook',
    'tw' => 'Поделиться в Twitter',
    'ml' => 'Поделиться в Mail.ru',
    'ln' => 'Поделиться в LinkedIn'
);
$out = '';

$tpl = isset($tpl) ? $tpl : '@CODE: <a href="[+url+]" title="[+name+]" onclick="javascript:window.open(this.href, \'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" rel="nofollow" target="_blank" class="[+class+]"></a>';
foreach($links as $item){
    if(isset($href[$item])){
        $tmpTpl = getkey($config, 'tpl-'.$item, $tpl);
        $out .= $modx->tpl->parseChunk($tmpTpl, array(
            'class' => $item,
            'url' => $href[$item],
            'e.title' => $config['titleEncode'],
            'e.url' => $config['urlEncode'],
            'name' => $title[$item]
        ));
    }
}
$wrapTpl = isset($wrapTpl) ? $wrapTpl : '@CODE: <noindex><div class="b-socbuttons">[+wrap+]</div></noindex>';
return $modx->tpl->parseChunk($wrapTpl, array('wrap' => $out));