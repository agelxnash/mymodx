;(function($){
    $(function() {
        $.sender = {
            lang: {
                errorSend: 'Не удалось получить ответ от сервера. Попробуйте повторить попытку позже',
                required: 'Это поле необходимо заполнить',
                errorLoad: 'Не удалось открыть страницу'
            },
            fancy: function(text, reload){
                $.event.trigger("onFancyOpen", [{content: text}]);
                $.fancybox({
                    content: text,
                    overlayColor:"#25437d",
                    overlayOpacity: 0.40,
                    autoWidth: true,
                    autoResize: true,
                    scrolling: 'auto',
                    ajax: {
                        dataType : 'html',
                        headers  : {
                            'X-fancyBox': true,
                            'X-ANSender': true
                        }
                    },
                    openSpeed : 450,
                    openEffect:'fade',
                    closeEffect:'fade',
                    closeSpeed : 50,
                    afterClose: function () {
                        if(reload){
                            location.reload();
                        }
                    }
                });
            },
            ajax: function(form, url){
                $.ajax({
                    url: $(form).attr('action'),
                    data: $(form).serialize() + '&' + $("body").dataSerialize('main'),
                    cache: false,
                    type: $(form).attr('method')
                }).done(function (responce) {
                    if(typeof responce.status !== 'undefined' && responce.status == 'ok'){
                        var el = (typeof responce.replace !== 'undefined') ? responce.replace : null;
                        if(el && $(el).length){
                            el = $(el);
                            el.fadeToggle('slow', function(){
                                el.replaceWith(responce.text)
                            }).fadeToggle('slow');
                        }else{
                            var url = (typeof responce.url !== 'undefined') ? responce.url : null;
                            if(url){
                                $(location).attr('href', url);
                            }else{
                                $.sender.fancy(responce.text, true);
                            }
                        }
                    }else{
                        if(typeof responce.text !== 'undefined'){
                           $.sender.fancy(responce.text);
                        }else{
                            $.ANMsg.error($.sender.lang.errorSend);
                        }
                    }
                }).fail(function () {
                    $.ANMsg.error($.sender.lang.errorSend);
                });
            }
        };
    });
})(jQuery);