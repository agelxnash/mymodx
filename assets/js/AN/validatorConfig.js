;(function($){
    $(function() {
        $.validatorConfig = {
            errorPlacement: function(error, element) { },
            highlight: function(element, errorClass, validClass) {
                var tagName = $.jHelper.lowerCase($.jHelper.tagName($(element)));
                var el;
                if(tagName == 'select'){
                    el = $(element).parent();
                }else{
                    el = $(element);
                }

                el.addClass(errorClass).removeClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                var tagName = $.jHelper.lowerCase($.jHelper.tagName($(element)));
                var el;
                if(tagName == 'select'){
                    el = $(element).parent();
                }else{
                    el = $(element);
                }

                el.removeClass(errorClass).addClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
            },
            /*rules: {
                email: {
                    required: true,
                    email: true
                },
                phone:{
                    required: true
                },
                user:{
                    required: true
                }
            },*/
            submitHandler: function(form){
                $.sender.ajax(form);
            }
        };
    });
})(jQuery);