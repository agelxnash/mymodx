(function($){
    $.ANcart = {
        flag: {
            'dels': false,
            'add': false
        },
        total:{
            'elem': 0,
            'summ': 0,
            'sale': 0
        },
        config: {
            'elem': '', /* Какой блок отправлять в корзину */
            'toElem': '', /* Куда отправлять блок с товаром */
            'addCartUrl': '', /* Куда отправлять запросы на пересчет суммы в корзине*/
            'itemId': '', /* ID элемета*/
            'itemCount': '', /* число элементов */
            'reloadCallback': '', /*callback функция для замены после добавления в корзину */
            'errorCallback': '', /* callback функция для вывода ошибок*/
            'typeReload': 'add' /* тип обновления корзины. add - добавить к предыдущему числу товаров. full - заменить число товаров */
        },
        flushTotal: function(){
            $.ANcart.total = {elem:0, summ:0, sale:0};
        },
        setConfig: function(cfg){ /* сохраняем настройки */
            $.extend(true, $.ANcart.config,cfg);
        },
        fly: function(cfg){ /* отправляем в корзину */
            $.ANcart.setConfig(cfg);
            if($($.ANcart.config.elem).length && $($.ANcart.config.toElem).length){
                var flyer = $($.ANcart.config.elem).clone();
                $(flyer)
                    .css({
                        'z-index' : '9999',
                        'position': 'absolute',
                        'top': $($.ANcart.config.elem).offset().top + "px",
                        'left': $($.ANcart.config.elem).offset().left + "px",
                    })
                    .appendTo($(this.config.elem))
                    .animate({
                        opacity: 0.4,
                        width: 50,
                        height: 50,
                        left: $($.ANcart.config.toElem).offset().left + ($($.ANcart.config.toElem).width() / 2),
                        top: $($.ANcart.config.toElem).offset().top,
                    },
                    600, function(){
                        $(this).remove();
                    });
				$.ANcart.reloadData();
            }else{
                console.error('[ANcart] В DOM дереве не найдены элементы elem: "' + $.ANcart.config.elem + '" или toElem: "' + $.ANcart.config.toElem + '"');
            }
        },
        reloadData: function(cfg){
            $.ANcart.setConfig(cfg);

            var data = {};
            data.id = $.ANcart.config.itemId;
            data.count = $.ANcart.config.itemCount;
            data.type = $.ANcart.config.typeReload;
            $.getJSON($.ANcart.config.addCartUrl, data).done(function (response) {
                data.response = response;
                data.element = $.ANcart.config.elem;
                if (!response.error) {
                    $.event.trigger("onReloadANCart", [data]);
                } else {
                    $.event.trigger("onErrorReloadANCart", [data]);
                }
            }).fail(function(jqxhr, textStatus, error) {
                console.log('Некорректный ответ от сервера: ' + JSON.stringify(jqxhr));
            });
        }
    };
})(jQuery);